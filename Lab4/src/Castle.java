import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.SimpleUniverse;
import javax.media.j3d.*;
import javax.swing.Timer;
import javax.vecmath.*;

public class Castle implements ActionListener {
    private float upperEyeLimit = 5.0f; 
    private float lowerEyeLimit = 1.0f; 
    private float farthestEyeLimit = 6.0f; 
    private float nearestEyeLimit = 3.0f; 

    private TransformGroup treeTransformGroup;
    private TransformGroup viewingTransformGroup;
    private Transform3D treeTransform3D = new Transform3D();
    private Transform3D viewingTransform = new Transform3D();
    private float angle = 0;
    private float eyeHeight;
    private float eyeDistance;
    private boolean descend = true;
    private boolean approaching = true;

    public static void main(String[] args) {
        new Castle();
    }

    private Castle() {
        Timer timer = new Timer(50, this);
        SimpleUniverse universe = new SimpleUniverse();

        viewingTransformGroup = universe.getViewingPlatform().getViewPlatformTransform();
        universe.addBranchGraph(createSceneGraph());

        eyeHeight = upperEyeLimit;
        eyeDistance = farthestEyeLimit;
        timer.start();
    }

    private BranchGroup createSceneGraph() {
        BranchGroup objRoot = new BranchGroup();

        treeTransformGroup = new TransformGroup();
        treeTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        buildCastleSkeleton();
        objRoot.addChild(treeTransformGroup);

        Background background = new Background(new Color3f(1.0f, 1.0f, 1.0f)); // white color
        BoundingSphere sphere = new BoundingSphere(new Point3d(0,0,0), 100000);
        background.setApplicationBounds(sphere);
        objRoot.addChild(background);

        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),100.0);
        Color3f light1Color = new Color3f(1.0f, 0.5f, 0.4f);
        Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);
        DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);
        light1.setInfluencingBounds(bounds);
        objRoot.addChild(light1);

       
        Color3f ambientColor = new Color3f(1.0f, 1.0f, 1.0f);
        AmbientLight ambientLightNode = new AmbientLight(ambientColor);
        ambientLightNode.setInfluencingBounds(bounds);
        objRoot.addChild(ambientLightNode);
        return objRoot;
    }

    private void buildCastleSkeleton() {
        Box body1 = CastleBody.getBody(0.1f, 1.2f);
        Transform3D body1T = new Transform3D();

        body1T.setTranslation(new Vector3f());
        TransformGroup body1TG = new TransformGroup();
        body1TG.setTransform(body1T);
        body1TG.addChild(body1);
        treeTransformGroup.addChild(body1TG);

        setOneLevelOfTowers(1f, 0.3f);


        Box body2 = CastleBody.getBody(0.2f, 0.6f);
        Transform3D body2T = new Transform3D();
        body2T.setTranslation(new Vector3f(.0f, .0f, 0.25f));
        TransformGroup body2TG = new TransformGroup();
        body2TG.setTransform(body2T);
        body2TG.addChild(body2);
        treeTransformGroup.addChild(body2TG);

        setWall();

        setCylinderTowers();

        setOneLevelOf4Fetches(1f, -0.125f);

    }

    private void setWall(){
        int primflags = Primitive.GENERATE_NORMALS + Primitive.GENERATE_TEXTURE_COORDS;
        Box wall1 = new Box(0.8f, 0.1f, 0.2f, primflags, CastleBody.getCubeTowersAppearence());

        Transform3D wall1T = new Transform3D();
        wall1T.setTranslation(new Vector3f(0f, 1.09f, 0.25f));
        TransformGroup body1TG = new TransformGroup();
        body1TG.setTransform(wall1T);
        body1TG.addChild(wall1);
        treeTransformGroup.addChild(body1TG);

        Box wall2 = new Box(0.8f, 0.1f, 0.2f, primflags, CastleBody.getCubeTowersAppearence());
        Transform3D wall2T = new Transform3D();
        wall2T.setTranslation(new Vector3f(0f, -1.09f, 0.25f));
        //wall2T.setRotation(new AxisAngle4d(0, 0, 1, Math.toRadians(90)));
        TransformGroup body2TG = new TransformGroup();
        body2TG.setTransform(wall2T);
        body2TG.addChild(wall2);
        treeTransformGroup.addChild(body2TG);

        Box wall3 = new Box(0.8f, 0.1f, 0.2f, primflags, CastleBody.getCubeTowersAppearence());
        Transform3D wall3T = new Transform3D();
        wall3T.setTranslation(new Vector3f(1.09f, 0f, 0.25f));
        wall3T.setRotation(new AxisAngle4d(0, 0, 1, Math.toRadians(90)));
        TransformGroup body3TG = new TransformGroup();
        body3TG.setTransform(wall3T);
        body3TG.addChild(wall3);
        treeTransformGroup.addChild(body3TG);

        Box wall4 = new Box(0.8f, 0.1f, 0.2f, primflags, CastleBody.getCubeTowersAppearence());
        Transform3D wall4T = new Transform3D();
        wall4T.setTranslation(new Vector3f(-1.09f, 0f, 0.25f));
        wall4T.setRotation(new AxisAngle4d(0, 0, 1, Math.toRadians(90)));
        TransformGroup body4TG = new TransformGroup();
        body4TG.setTransform(wall4T);
        body4TG.addChild(wall4);
        treeTransformGroup.addChild(body4TG);
    }

    private void setUpperProtectFetches(){
        float distanceFromCentre = 0.625f;
        float zPos = 0.38f; // 0.33f
        TransformGroup protectFetch1 = CastleBody.getProtectFetch(distanceFromCentre, .0f, zPos, true);
        treeTransformGroup.addChild(protectFetch1);
        TransformGroup protectFetch2 = CastleBody.getProtectFetch(-distanceFromCentre, .0f, zPos, true);
        treeTransformGroup.addChild(protectFetch2);
        TransformGroup protectFetch3 = CastleBody.getProtectFetch(.0f, distanceFromCentre, zPos, false);
        treeTransformGroup.addChild(protectFetch3);
        TransformGroup protectFetch4 = CastleBody.getProtectFetch(.0f, -distanceFromCentre, zPos, false);
        treeTransformGroup.addChild(protectFetch4);
    }

    private void setCylinderTowers(){
        float cylTowDistFromCentre = 0.5f;
        TransformGroup cylinderTower1 = CastleBody.getCylinderTower(2f, .0f, .0f);
        treeTransformGroup.addChild(cylinderTower1);

    }

    private void setOneLevelOf4Fetches(float distanceFromCentre, float height){
        TransformGroup fourFetches1 = CastleBody.getFourFetches();
        Transform3D tower1T = new Transform3D();
        tower1T.setTranslation(new Vector3f(distanceFromCentre, distanceFromCentre, height));
        fourFetches1.setTransform(tower1T);
        treeTransformGroup.addChild(fourFetches1);

        TransformGroup fourFetches2 = CastleBody.getFourFetches();
        Transform3D tower2T = new Transform3D();
        tower2T.setTranslation(new Vector3f(-distanceFromCentre, -distanceFromCentre, height));
        fourFetches2.setTransform(tower2T);
        treeTransformGroup.addChild(fourFetches2);

        TransformGroup fourFetches3 = CastleBody.getFourFetches();
        Transform3D tower3T = new Transform3D();
        tower3T.setTranslation(new Vector3f(distanceFromCentre, -distanceFromCentre, height));
        fourFetches3.setTransform(tower3T);
        treeTransformGroup.addChild(fourFetches3);

        TransformGroup fourFetches4 = CastleBody.getFourFetches();
        Transform3D tower4T = new Transform3D();
        tower4T.setTranslation(new Vector3f(-distanceFromCentre, distanceFromCentre, height));
        fourFetches4.setTransform(tower4T);
        treeTransformGroup.addChild(fourFetches4);
    }

    private void setOneLevelOfTowers(float distanceFromCentre, float height){
        Box tower1 = CastleBody.getTower();
        Transform3D tower1T = new Transform3D();
        tower1T.setTranslation(new Vector3f(distanceFromCentre, distanceFromCentre, height));
        TransformGroup tower1TG = new TransformGroup();
        tower1TG.setTransform(tower1T);
        tower1TG.addChild(tower1);
        treeTransformGroup.addChild(tower1TG);

        Box tower2 = CastleBody.getTower();
        Transform3D tower2T = new Transform3D();
        tower2T.setTranslation(new Vector3f(-distanceFromCentre, -distanceFromCentre, height));
        TransformGroup tower2TG = new TransformGroup();
        tower2TG.setTransform(tower2T);
        tower2TG.addChild(tower2);
        treeTransformGroup.addChild(tower2TG);

        Box tower3 = CastleBody.getTower();
        Transform3D tower3T = new Transform3D();
        tower3T.setTranslation(new Vector3f(distanceFromCentre, -distanceFromCentre, height));
        TransformGroup tower3TG = new TransformGroup();
        tower3TG.setTransform(tower3T);
        tower3TG.addChild(tower3);
        treeTransformGroup.addChild(tower3TG);

        Box tower4 = CastleBody.getTower();
        Transform3D tower4T = new Transform3D();
        tower4T.setTranslation(new Vector3f(-distanceFromCentre, distanceFromCentre, height));
        TransformGroup tower4TG = new TransformGroup();
        tower4TG.setTransform(tower4T);
        tower4TG.addChild(tower4);
        treeTransformGroup.addChild(tower4TG);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        float delta = 0.03f;

        treeTransform3D.rotZ(angle);
        treeTransformGroup.setTransform(treeTransform3D);
        angle += delta;

        if (eyeHeight > upperEyeLimit){
            descend = true;
        }else if(eyeHeight < lowerEyeLimit){
            descend = false;
        }
        if (descend){
            eyeHeight -= delta;
        }else{
            eyeHeight += delta;
        }

        // change camera distance to the scene
        if (eyeDistance > farthestEyeLimit){
            approaching = true;
        }else if(eyeDistance < nearestEyeLimit){
            approaching = false;
        }
        if (approaching){
            eyeDistance -= delta;
        }else{
            eyeDistance += delta;
        }

        Point3d eye = new Point3d(eyeDistance, eyeDistance, eyeHeight); // spectator's eye
        Point3d center = new Point3d(.0f, .0f ,0.5f); // sight target
        Vector3d up = new Vector3d(.0f, .0f, 1.0f);; // the camera frustum
        viewingTransform.lookAt(eye, center, up);
        viewingTransform.invert();
        viewingTransformGroup.setTransform(viewingTransform);
    }
}
