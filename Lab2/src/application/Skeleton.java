package application;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Skeleton extends JPanel implements ActionListener {

	private static int maxWidth;
	private static int maxHeight;
	private double angle = 0;
	
	private double scale = 1;
	private double delta = 0.01;
	// ��� �������� ����
	private double dx = 1;
	private double dy = 1;
	int tx = -150;
	int ty = 150;
	private int frametop = 150;
	
	
	
	Timer timer;
	
	public Skeleton() {
		// ������ ������������ ���� �� 10 ��
		timer = new Timer(10, this);
		timer.start();
	}
	
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
												RenderingHints.VALUE_ANTIALIAS_ON);
		rh.put(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g2d.setRenderingHints(rh);
		
		g2d.setBackground(Color.black);
		g2d.clearRect(0, 0, maxWidth, maxHeight);
		
		// ������� � ����������� ������ 1
		g2d.setPaint(Color.yellow);
		double points[][] = {
				{166, 14}, {140, 85}, {55, 85},
				{131, 130}, {105, 201}, {180, 162},
				{268, 210}, {230, 133}, {300, 88}, {207, 85}
		};
		
		GeneralPath star = new GeneralPath();
		g2d.translate(0,0);
		star.moveTo(points[0][0], points[0][1]);
		for (int k = 1; k < points.length; k++) {
			star.lineTo(points[k][0], points[k][1]);
		}
		star.closePath();
		g2d.fill(star);
		
		g2d.setPaint(Color.yellow);
		BasicStroke bs2 = new BasicStroke(8, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
		g2d.setStroke(bs2);
		g2d.drawLine(175, 156, 175, 234);
		
		g2d.setPaint(new Color(0,128,255));
		GeneralPath pentagram = new GeneralPath();
		pentagram.moveTo(points[1][0], points[1][1]);
		for(int i = 3; i < points.length; i++){
			if(i%2 != 0) {
				pentagram.lineTo(points[i][0], points[i][1]);
			}
		}
		pentagram.closePath();
		g2d.fill(pentagram);
		
		//����� ��������
		g2d.translate(0, 2*maxHeight/5);
		BasicStroke fstroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
		g2d.setStroke(fstroke);
		
		GradientPaint gp = new GradientPaint(5, 25,
		Color.CYAN, 20, 2, Color.BLUE, true);
		g2d.setPaint(gp);
		g2d.draw(new Ellipse2D.Double(50, 40, 120, 80));
		g2d.fillOval(250, 40, 100, 100);
		
		
		double fPoints[][] = {
				{220, 150}, {170, 250}, {40, 250}, {120, 330}, {320, 330}, {420, 250}, {270, 250}
		};
		
		
		GradientPaint grad = new GradientPaint(2, 6,
				Color.black, 10, 2, Color.gray, true);
				g2d.setPaint(grad);
		
		GeneralPath ship = new GeneralPath();
		ship.moveTo(fPoints[0][0], fPoints[0][1]);
		
		for(int i = 1; i < fPoints.length; i++){
				ship.lineTo(fPoints[i][0], fPoints[i][1]);
		}
		
		ship.closePath();
		g2d.fill(ship);
		
		//�����
		g2d.translate(0, -2*maxHeight/5);
		//g2d.translate(2*maxWidth/3, 0);
		g2d.translate(2*maxWidth/3, maxHeight/2);
		
		BasicStroke frame = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 5);
		g2d.setPaint(Color.white);
		g2d.setStroke(frame);
		
		int framePoints [][] = {{-300, -300}, {-300, 300}, {300, 300}, {300, -300}, {-300, -300}};
		
		for(int i = 0; i<framePoints.length-1; i++) {
			g2d.drawLine(framePoints[i][0], framePoints[i][1], framePoints[i+1][0], framePoints[i+1][1]);
		}
		
		//��������� ������  � ����������� 1
		
		
		for (int i = 0; i < points.length; i++) {
			points[i][0] -= 165; 
			points[i][1] -= 135;
		}
		
		
		g2d.setPaint(Color.yellow);
		
		GeneralPath star2 = new GeneralPath();
		g2d.translate(0,0);
		star2.moveTo(points[0][0], points[0][1]);
		for (int k = 1; k < points.length; k++) {
			star2.lineTo(points[k][0], points[k][1]);
		}
		star.closePath();
		
		//g2d.rotate(angle, star.getBounds2D().getCenterX(),star.getBounds2D().getCenterY());
		//g2d.scale(scale, 0.99);
		g2d.translate(ty, tx);
		
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
				(float)scale));
		
		g2d.fill(star2);
		
		g2d.setPaint(Color.yellow);
		g2d.setStroke(bs2);
		g2d.drawLine(10, 21, 10, 100);
		
		g2d.setPaint(new Color(0,128,255));
		GeneralPath pentagram2 = new GeneralPath();
		pentagram2.moveTo(points[1][0], points[1][1]);
		for(int i = 3; i < points.length; i++){
			if(i%2 != 0) {
				pentagram2.lineTo(points[i][0], points[i][1]);
			}
		}
		pentagram.closePath();
		g2d.fill(pentagram2);
		
	}
	
	public static void main(String[] args) {
	
		JFrame frame = new JFrame("Lab2 Lanovskyi");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1200, 750);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.add(new Skeleton());
		frame.setVisible(true);
		
		Dimension size = frame.getSize();
		Insets insets = frame.getInsets();
		maxWidth = size.width - insets.left - insets.right - 1;
		maxHeight = size.height - insets.top - insets.bottom - 1;
	}
	
	public void actionPerformed(ActionEvent e) {
		
		
		if ( scale < 0.01 ) {
			delta = -delta;
		} else if (scale > 0.99) {
			delta = -delta;
		}
		
		if(tx == -frametop && ty > -frametop) {
			dx = 0;
			dy = -1;
		}
		if(ty == -frametop && tx < frametop) {
			dx = 1;
			dy = 0;
		}
		if(tx == frametop && ty < frametop) {
			dx = 0;
			dy = 1;
		}
		if(ty == frametop && tx > -frametop) {
			dx = -1;
			dy = 0;
		}
		
		scale += delta;
		angle += 0.01;
		tx += dx;
		ty += dy;
		repaint();
	}
}
