package application;
	
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.shape.*;
import javafx.scene.paint.Color;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Group root = new Group();
			Scene scene = new Scene(root, 420, 250);
			
			scene.setFill(Color.RED);
			
			Polygon star = new Polygon(166, 14, 
										140, 85, 
										55, 85,
										131, 130,
										105, 201, 
										180, 162,
										268, 210,
										230, 133,
										300, 88,
										207, 85);
			root.getChildren().add(star);
			star.setFill(Color.YELLOW);
			
			Rectangle rect = new Rectangle(172, 155, 8, 79);
			root.getChildren().add(rect);
			rect.setArcWidth(7);
			rect.setArcHeight(7);
			rect.setFill(Color.YELLOW);
			
			Polygon pentagram = new Polygon(140, 85,
					131, 130,
					180, 162,
					230, 133,
					207, 85);
			root.getChildren().add(pentagram);
			pentagram.setFill(Color.rgb(0,128,255));
			
			/*Circle[] circles = {new Circle(166, 14 , 1),
								new Circle(55, 85 , 1),
								new Circle(105, 201 , 1),
								new Circle(268, 210 , 1),
								new Circle(300, 88 , 1)};
			root.getChildren().addAll(circles);
			for(int i = 0; i < circles.length; i++) {
				circles[i].setFill(Color.WHITE);
			}*/
			
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
